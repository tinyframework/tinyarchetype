package ${package}.demo.service;

import ${package}.demo.pojo.SendRequest;
import ${package}.demo.pojo.SendResponse;

/**
 * 发送消息的服务
 * @author yancheng11334
 *
 */
public interface SendMessageService {

	public SendResponse sendMessage(SendRequest request);
}
