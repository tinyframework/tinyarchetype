package ${package}.demo.service;

import ${package}.demo.pojo.ReceiveRequest;
import ${package}.demo.pojo.ReceiveResponse;

/**
 * 接收消息服务
 * @author yancheng11334
 *
 */
public interface ReceiveMessageService {

	public ReceiveResponse receive(ReceiveRequest request);
}
