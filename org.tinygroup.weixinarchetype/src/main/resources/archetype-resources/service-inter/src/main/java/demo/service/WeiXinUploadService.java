package ${package}.demo.service;

import ${package}.demo.pojo.UploadRequest;
import ${package}.demo.pojo.UploadResponse;

/**
 * 上传服务
 * @author yancheng11334
 *
 */
public interface WeiXinUploadService {

	public UploadResponse upload(UploadRequest request);
}
