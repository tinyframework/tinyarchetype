package ${package}.listener;

import org.tinygroup.weblayer.listener.ServletContextHolder;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class TinyServletContextSetListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContextHolder.setServletContext(sce.getServletContext());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContextHolder.clear();
    }
}
